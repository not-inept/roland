# Roland

Roland is a discord bot for rolling dice and keeping track of skills.

## Rolling with Roland
To roll, you can use Roland with `!20` or `!d20` or `!1d20` to roll a single d20. The generalized usage is `!<n>d<s>`, where `<n>` is the number of dice and `<s>` is the number of sides on each die. You can do multiple rolls separated with spaces.

## Skills and Contexts
To use skills, you'll first need to setup a context using `!register <name>` in a channel of your choice. Only the person who creates a context can add it to other channels. Only one context can be added per channel, currently. Once a context is registered to a channel, use `!set <skill> <value>` to set a skill. When rolling, list the relevant skills after the dice description. For example: `!20 agility`.

## Advantage and Disadvantage
To roll with advantage or disadvantage you can add an `a` or `d` to the end of the dice description. For example: `!2d6a 2d6d` to roll two d6 with advantage and two with disadvantage. 