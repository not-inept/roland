package main

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/Necroforger/gomoji"
	"github.com/sirupsen/logrus"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/not-inept/roland/cmd"
)

func myMention(s *discordgo.Session) string {
	// Mentions show up as <@id> in messages where they aren't parsed out
	return fmt.Sprintf("<@%s>", s.State.User.ID)
}

// MessageCreate will be called (due to AddHandler) every time a new
// message is created on any channel that the authenticated bot has access to.
func MessageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	// Ignore all messages created by the bot itself
	if m.Author.ID == s.State.User.ID {
		return
	}

	// All commands are prefixed by '!' or @botname
	var content string
	if strings.HasPrefix(m.Content, "!") {
		content = strings.TrimPrefix(m.Content, "!")
	} else if strings.HasPrefix(m.Content, myMention(s)+" ") {
		content = strings.TrimPrefix(m.Content, myMention(s)+" ")
	} else {
		return
	}

	fields := strings.Fields(strings.ToLower(content))
	if len(fields) == 0 {
		return
	}
	logrus.Infof("Command %s invoked.", fields[0])
	switch fields[0] {
	case "help":
		cmd.Help(s, m)
	case "set":
		if len(fields) != 3 {
			s.MessageReactionAdd(m.ChannelID, m.ID, gomoji.Format(":thumbsdown:"))
			return
		}
		value, err := strconv.Atoi(fields[2])
		if err != nil {
			s.MessageReactionAdd(m.ChannelID, m.ID, gomoji.Format(":thumbsdown:"))
			return
		}
		cmd.Set(s, m, &users, &contexts, fields[1], value)
	case "register":
		if len(fields) != 2 {
			s.MessageReactionAdd(m.ChannelID, m.ID, gomoji.Format(":thumbsdown:"))
			return
		}
		cmd.Register(s, m, &contexts, fields[1])
	default:
		cmd.Default(s, m, fields, &users, &contexts)
	}
}
