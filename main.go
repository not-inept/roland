package main

import (
	"math/rand"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/sirupsen/logrus"
	"gitlab.com/not-inept/roland/util"
)

var users sync.Map
var contexts sync.Map

func main() {
	logrus.Info("Starting up Roland.")
	rand.Seed(time.Now().UnixNano())

	config := util.LoadConfig("roland.json")

	logrus.Info("Loading users...")
	loaded, err := util.LoadUsers(config.UsersPath, &users)
	if err != nil {
		logrus.Error("Failed to load users!")
	}
	logrus.Infof("Loaded %d users.", loaded)

	logrus.Info("Loading contexts...")
	loaded, err = util.LoadContexts(config.ContextsPath, &contexts)
	if err != nil {
		logrus.Error("Failed to load contexts!")
	}
	logrus.Infof("Loaded %d contexts.", loaded)

	discord, err := discordgo.New("Bot " + config.Token)
	if err != nil {
		logrus.Fatal("Error creating Discord session,", err)
		return
	}

	discord.AddHandler(MessageCreate)
	err = discord.Open()
	if err != nil {
		logrus.Fatal("Error opening connection,", err)
		return
	}

	// go WatchManager(discord, 5*time.Minute)
	// go usersBackup(config.UsersPath)

	// // Wait here until CTRL-C or other term signal is received.
	logrus.Infof("Roland is now running.  Press CTRL-C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	// // Cleanly close down the Discord session.
	discord.Close()

	logrus.Info("Storing users...")
	saved, err := util.SaveUsers(config.UsersPath, &users)
	if err != nil {
		logrus.Fatal("Failed to save users!")
	}
	logrus.Infof("Saved %d users.", saved)

	logrus.Info("Storing contexts...")
	saved, err = util.SaveContexts(config.ContextsPath, &contexts)
	if err != nil {
		logrus.Fatal("Failed to save contexts!")
	}
	logrus.Infof("Saved %d contexts.", saved)
}
