package cmd

import (
	"sync"

	"github.com/Necroforger/gomoji"
	"github.com/bwmarrin/discordgo"
)

// Register allows a user to register a channel to a context
func Register(s *discordgo.Session, m *discordgo.MessageCreate, contexts *sync.Map, field string) {
	// TODO: Check if context already exists, make sure it is okay to move forward
	contexts.Store(m.ChannelID, m.Author.ID+"-"+field)
	s.MessageReactionAdd(m.ChannelID, m.ID, gomoji.Format(":thumbsup:"))
}
