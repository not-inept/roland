package cmd

import (
	"errors"
	"sync"

	"github.com/Necroforger/gomoji"
	"github.com/bwmarrin/discordgo"
	"gitlab.com/not-inept/roland/util"
)

// Set sets a skill in a channel's context
func Set(s *discordgo.Session, m *discordgo.MessageCreate, users *sync.Map, contexts *sync.Map, skill string, value int) error {
	contextRaw, ok := contexts.Load(m.ChannelID)
	if !ok {
		s.ChannelMessageSend(m.ChannelID, "This channel doesn't currently have a context registered to it! Use `!register <context>` to fix that.")
		return errors.New("Channel does not have a registered context")
	}
	context := contextRaw.(string)

	userRaw, ok := users.Load(m.Author.ID)
	var user *util.User
	if ok {
		user = userRaw.(*util.User)
	} else {
		user = &util.User{
			UserID:   m.Author.ID,
			Contexts: make(map[string]util.ContextSkills),
		}
	}
	_, ok = user.Contexts[context]
	if !ok {
		user.Contexts[context] = make(util.ContextSkills)
	}
	user.Contexts[context][skill] = value
	users.Store(m.Author.ID, user)
	s.MessageReactionAdd(m.ChannelID, m.ID, gomoji.Format(":thumbsup:"))
	return nil
}
