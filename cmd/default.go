package cmd

import (
	"errors"
	"fmt"
	"strings"
	"sync"

	"github.com/Necroforger/gomoji"
	"github.com/bwmarrin/discordgo"
	"github.com/sirupsen/logrus"
	"gitlab.com/not-inept/roland/dice"
	"gitlab.com/not-inept/roland/util"
)

func writeRoll(dx dice.Dice, str *strings.Builder) {
	for i, result := range dx.LastRoll {
		str.WriteString(fmt.Sprintf("\n\t[%d] %s: %d", i, dx.Die(i).String(), result))
	}
}

func parseRolls(fields []string) ([]rollData, error) {
	var rollDatum []rollData
	for _, field := range fields {
		field = strings.Replace(field, gomoji.Format(":game_die:"), "d6", 1)
		dx, err := dice.Parse(field)
		if err != nil {
			if len(rollDatum) > 0 {
				rollDatum[len(rollDatum)-1].Modifiers = append(rollDatum[len(rollDatum)-1].Modifiers, field)
			} else {
				logrus.Error(err)
				return []rollData{}, fmt.Errorf("Invalid roll arguments passed: %v", fields)
			}
		} else {
			rollDatum = append(rollDatum, rollData{Dx: dx})
		}
	}
	return rollDatum, nil
}

func totalWithAdvantage(response *strings.Builder, dx dice.Dice) int {
	rollTotal := dx.Total()
	if dx.Advantage != 0 {
		dx.Roll()
		response.WriteString("\nAnd I rolled again, look at this:")
		writeRoll(dx, response)
		if rollTotal > dx.Total() {
			if dx.Advantage < 0 {
				rollTotal = dx.Total()
			}
		} else {
			if dx.Advantage > 0 {
				rollTotal = dx.Total()
			}
		}
	}
	return rollTotal
}

func getModifiers(users *sync.Map, contexts *sync.Map, m *discordgo.MessageCreate, modifiers []string) ([]int, error) {
	// If we have no modifiers
	if len(modifiers) < 1 {
		return []int{}, nil
	}
	// Attempt to acquire context
	contextRaw, ok := contexts.Load(m.ChannelID)
	if !ok {
		return []int{}, errors.New("No context associated with channel")
	}
	context := contextRaw.(string)
	// Get user skills for context
	userRaw, ok := users.Load(m.Author.ID)
	if !ok {
		return []int{}, errors.New("No user associated with message author")
	}
	user := userRaw.(*util.User)
	contextSkills, ok := user.Contexts[context]
	if !ok {
		return []int{}, errors.New("No skills associated with message author in context")
	}
	// Associate context skills with values
	var modifierValues []int
	for _, modifier := range modifiers {
		skillValue, ok := contextSkills[modifier]
		if !ok {
			return []int{}, fmt.Errorf("User does not have skill: %s", modifier)
		}
		skillValue = (skillValue - 10) / 2
		modifierValues = append(modifierValues, skillValue)

	}
	return modifierValues, nil
}

func writeTotalWithModifiers(response *strings.Builder, rollTotal int, modifiers []int) {
	response.WriteString(fmt.Sprintf("\nThe total was: %d", rollTotal))
	if len(modifiers) > 0 {
		sum := 0
		var rollValue strings.Builder
		fmt.Fprintf(&rollValue, "%d", rollTotal)
		for _, modifier := range modifiers {
			sum += modifier
			sign := "+"
			if modifier < 0 {
				sign = ""
			}
			fmt.Fprintf(&rollValue, "%s%d", sign, modifier)
		}
		fmt.Fprintf(response, "\nWith your skills, the total is: %d (%s)", rollTotal+sum, rollValue.String())
	}

}

// rolLData is a roll and any modifiers passed to affect it
type rollData struct {
	Dx        dice.Dice
	Modifiers []string
}

// Default is the default function for Roland, roll some dice!
func Default(s *discordgo.Session, m *discordgo.MessageCreate, fields []string, users *sync.Map, contexts *sync.Map) {
	if len(fields) == 0 {
		s.MessageReactionAdd(m.ChannelID, m.ID, gomoji.Format(":thumbsdown:"))
		return
	}

	rollDatum, err := parseRolls(fields)
	if err != nil || len(rollDatum) <= 0 {
		s.MessageReactionAdd(m.ChannelID, m.ID, gomoji.Format(":thumbsdown:"))
		return
	}

	for _, roll := range rollDatum {

		var response strings.Builder
		roll.Dx.Roll()
		response.WriteString(m.Author.Mention() + " I've rolled for you, here's what chance found:")
		writeRoll(roll.Dx, &response)
		rollTotal := totalWithAdvantage(&response, roll.Dx)
		modifiers, err := getModifiers(users, contexts, m, roll.Modifiers)
		if err != nil {
			logrus.Error(err)
			s.MessageReactionAdd(m.ChannelID, m.ID, gomoji.Format(":thumbsdown:"))
			return
		}
		writeTotalWithModifiers(&response, rollTotal, modifiers)
		_, err = s.ChannelMessageSend(m.ChannelID, response.String())
		if err != nil {
			logrus.Error(err)
			s.MessageReactionAdd(m.ChannelID, m.ID, gomoji.Format(":thumbsdown:"))
			return
		}
	}
}
