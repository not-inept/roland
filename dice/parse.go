package dice

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/sirupsen/logrus"
)

// Parse advantage/disadvantage out of a raw string
func parseAdvantage(raw string) (string, int) {
	rawTrimmed := strings.TrimSuffix(strings.TrimSuffix(strings.TrimSuffix(raw, "advantage"), "adv"), "a")
	if rawTrimmed != raw {
		lowerRaw, lowerAdv := parseAdvantage(rawTrimmed)
		return lowerRaw, lowerAdv + 1
	}
	rawTrimmed = strings.TrimSuffix(strings.TrimSuffix(strings.TrimSuffix(raw, "disadvantage"), "dis"), "d")
	if rawTrimmed != raw {
		lowerRaw, lowerAdv := parseAdvantage(rawTrimmed)
		return lowerRaw, lowerAdv - 1
	}
	return raw, 0
}

// Parse parses a string into a dice type and roll quantity
// The format for dice is <n>d<s>
// In that, <n> is the number of dice to roll and <s> is the number of
// sides
func Parse(raw string) (Dice, error) {
	dx := Dice{}
	raw, dx.Advantage = parseAdvantage(raw)
	split := strings.Split(raw, "d")
	if len(split) == 1 {
		sides, err := strconv.Atoi(split[0])
		if err != nil || sides < 1 {
			logrus.Errorf("Couldn't parse dice with sides: %s", split[0])
			return Dice{}, fmt.Errorf("Unable to parse dice with raw: %s", raw)
		}
		dx.Add(NewDice(sides, 1))
	} else if len(split) == 2 {
		sides, err := strconv.Atoi(split[1])
		if err != nil || sides < 1 {
			logrus.Errorf("Couldn't parse dice with sides: %s", split[1])
			return Dice{}, fmt.Errorf("Unable to parse dice with raw: %s", raw)
		}
		quantity := 1
		if split[0] != "" {
			quantity, err = strconv.Atoi(split[0])
			if err != nil || quantity < 1 {
				logrus.Errorf("Couldn't parse dice with quantity: %s", split[0])
				return Dice{}, fmt.Errorf("Unable to parse dice with raw: %s", raw)
			}
		}
		dx.Add(NewDice(sides, quantity))
	} else {
		logrus.Errorf("Unable to parse dice with raw: %s", raw)
		return Dice{}, fmt.Errorf("Unable to parse dice with raw: %s", raw)
	}
	return dx, nil
}
