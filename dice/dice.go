package dice

import "fmt"

// Dice, a group of die
type Dice struct {
	Set       []Die
	LastRoll  []int
	Advantage int
}

// NewDice makes a new set of dice
func NewDice(sides int, quantity int) Dice {
	dx := Dice{}
	for i := 0; i < quantity; i++ {
		dx.Set = append(dx.Set, Die{
			Sides: sides,
		})
	}
	return dx
}

// Get the Die at index i
func (dx *Dice) Die(i int) Die {
	return dx.Set[i]
}

// String produces a string name for type of die
func (dx *Dice) String() string {
	// TODO: Don't assume they are all the same
	return fmt.Sprintf("d%d", dx.Set[0].Sides)
}

// Roll all dice and return a set of results
func (dx *Dice) Roll() []int {
	var rolls []int
	for _, d := range dx.Set {
		rolls = append(rolls, d.Roll())
	}
	dx.LastRoll = rolls
	return rolls
}

// Total gets the total value of the last dice roll
func (dx *Dice) Total() int {
	var total int
	for _, result := range dx.LastRoll {
		total += result
	}
	return total
}

// Add combines two sets of dice
func (dx *Dice) Add(dy Dice) {
	dx.Set = append(dx.Set, dy.Set...)
	dx.LastRoll = append(dx.LastRoll, dy.LastRoll...)
}
