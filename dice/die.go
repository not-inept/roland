package dice

import (
	"fmt"
	"math/rand"
)

// Die contains metadata for
type Die struct {
	Sides int
}

// Roll rolls the die, it expects rand to be seeded properly!
func (d *Die) Roll() int {
	return rand.Intn(d.Sides) + 1
}

func (d Die) String() string {
	return fmt.Sprintf("D%d", d.Sides)
}
