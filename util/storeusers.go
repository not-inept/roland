package util

import (
	"encoding/json"
	"io/ioutil"
	"sync"

	"github.com/sirupsen/logrus"
)

// SaveUsers saves the sync.Map to disk, as sync.Map itself
// isn't able to be marshalled
func SaveUsers(filePath string, users *sync.Map) (int, error) {
	tmpMap := make(map[string]*User)
	var savedUsers int
	users.Range(func(k, v interface{}) bool {
		userID := k.(string)
		userObj := v.(*User)
		tmpMap[userID] = userObj

		savedUsers++
		return true
	})
	mapBytes, err := json.Marshal(tmpMap)
	if err != nil {
		logrus.Error(err)
		return 0, err
		// TODO should probably catch this, as otherwise everything is lost
	}
	err = ioutil.WriteFile(filePath, mapBytes, 0777)
	if err != nil {
		logrus.Error(err)
		return 0, err
		// TODO should probably catch this, as otherwise everything is lost
	}
	return savedUsers, nil
}

// LoadUsers loads the sync.Map from disk, as sync.Map itself
// isn't able to be unmarshalled into
func LoadUsers(filePath string, users *sync.Map) (int, error) {
	var tmpMap map[string]User
	var loadedUsers int
	data, err := ioutil.ReadFile(filePath)
	if err != nil {
		logrus.Errorf("Error: %v", err)
		return 0, err
	}
	if err := json.Unmarshal(data, &tmpMap); err != nil {
		logrus.Error(err)
		return 0, err
	}
	for key := range tmpMap {
		user := tmpMap[key]
		users.Store(key, &user)
		loadedUsers++
	}
	return loadedUsers, nil
}
