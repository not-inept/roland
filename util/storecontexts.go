package util

import (
	"encoding/json"
	"io/ioutil"
	"sync"

	"github.com/sirupsen/logrus"
)

// SaveContexts saves the sync.Map to disk, as sync.Map itself
// isn't able to be marshalled
func SaveContexts(filePath string, contexts *sync.Map) (int, error) {
	tmpMap := make(map[string]string)
	var savedContexts int
	contexts.Range(func(k, v interface{}) bool {
		channelID := k.(string)
		contextID := v.(string)
		tmpMap[channelID] = contextID

		savedContexts++
		return true
	})
	mapBytes, err := json.Marshal(tmpMap)
	if err != nil {
		logrus.Error(err)
		return 0, err
		// TODO should probably catch this, as otherwise everything is lost
	}
	err = ioutil.WriteFile(filePath, mapBytes, 0777)
	if err != nil {
		logrus.Error(err)
		return 0, err
		// TODO should probably catch this, as otherwise everything is lost
	}
	return savedContexts, nil
}

// LoadContexts loads the sync.Map from disk, as sync.Map itself
// isn't able to be unmarshalled into
func LoadContexts(filePath string, contexts *sync.Map) (int, error) {
	var tmpMap map[string]string
	var loadedContexts int
	data, err := ioutil.ReadFile(filePath)
	if err != nil {
		logrus.Errorf("Error: %v", err)
		return 0, err
	}
	if err := json.Unmarshal(data, &tmpMap); err != nil {
		logrus.Error(err)
		return 0, err
	}
	for channelID := range tmpMap {
		context := tmpMap[channelID]
		contexts.Store(channelID, context)
		loadedContexts++
	}
	return loadedContexts, nil
}
