package util

// Config contains Roland's basic configuration info for connecting with discord and saving data
type Config struct {
	Token        string `json:"token"`
	UsersPath    string `json:"users_path"`
	ContextsPath string `json:"contexts_path"`
}

// ContextSkills is a map of 'skillname' and 'value'
type ContextSkills map[string]int

// User is a Rolland user
type User struct {
	UserID   string                   `json:"user_id"`
	Contexts map[string]ContextSkills `json:"skills"`
}
